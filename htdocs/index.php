<?php
require_once('views/app_fns.php');

html_fns_menu();
html_fns_header();

$page = isset($_GET['page']) ? $_GET['page'] : 'home';

html_fns_body($page);

html_fns_footer();
?>