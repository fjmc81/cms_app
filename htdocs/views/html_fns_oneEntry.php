<?php

function html_fns_oneEntry()
{

    // get ID of the product to be read
    $id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: missing ID.');
    
    // core configuration
    include_once "inner/config/core.php";
    // include models
    include_once "inner/config/database.php";
    include_once "inner/models/Entry.php";
    
    // get database connection
    $database = new Database();
    $db = $database->getConnection();
    
    // prepare objects
    $entry = new Entry();
    
    // set ID property of entry to be read
    $entry->id = $id;
    
    // read the details of entry to be read
    $entry->readOne($db);

    // set page headers
    $page_title = "Read One Entry";

    // HTML table for displaying a entries details
    echo "<table class='table'>";
    
        echo "<tr>";
            echo "<td>Title</td>";
            echo "<td>{$entry->title}</td>";
        echo "</tr>";

        echo "<tr>";
            echo "<td>Excerpt</td>";
            echo "<td>{$entry->excerpt}</td>";
        echo "</tr>";
    
        echo "<tr>";
            echo "<td>Content</td>";
            echo "<td>{$entry->content}</td>";
        echo "</tr>";
    
    echo "</table>";
        echo "<div class='p-2'>";
        echo "<a href='index.php' class='btn btn-primary pull-right'>
                <span class='glyphicon glyphicon-list'></span> Read All Entries
            </a>";
        echo "</div>";
    echo "</div>";
}
?>

