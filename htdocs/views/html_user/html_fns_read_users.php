<?php
 
// check if logged in as admin
//include_once "inner/controllers/read_users.php";

 
function html_fns_read_users()
{

    try {

        // include models
        include_once "inner/config/database.php";
        include_once "inner/models/Entry.php";
        include_once "inner/models/User.php";
        
        // get database connection
        $database = new Database();
        $db = $database->getConnection();

        // initialize objects
        $user = new User($db);
        
        $q = 'SELECT id, username, email, pass, access_level 
                FROM users  
                ORDER BY id DESC
                '; 

        $stmt = $db->prepare( $q );

        // bind given attribute value
        $stmt->bindParam(1, $entry->id);
        $stmt->bindParam(1, $entry->username);
        $stmt->bindParam(1, $entry->email);
        $stmt->bindParam(1, $entry->pass);
        $stmt->bindParam(1, $entry->access_level);
    
        // execute the query
        $stmt->execute();

        // display the table if the number of users retrieved was greater than zero
        if($stmt){
        
            echo "<table class='table table-hover table-responsive table-bordered'>";
        
            // table headers
            echo "<tr>";
                echo "<th>Id</th>";
                echo "<th>Username</th>";
                echo "<th>Email</th>";
                echo "<th>Password</th>";
                echo "<th>Access Level</th>";
            echo "</tr>";
            
            $stmt->setFetchMode(PDO::FETCH_CLASS, 'User');

            while ($user = $stmt->fetch()) {
                if($user->getUsername() == $_SESSION['username']){
                // display user details
                echo "<tr>";
                    echo "<td>{$user->getId()}</td>";
                    echo "<td>{$user->getUsername()}</td>";
                    echo "<td>{$user->getEmail()}</td>";
                    echo "<td>{$user->getPass()}</td>";
                    echo "<td>{$user->getAccess_level()}</td>";
                echo "</tr>";
                }
            }
        
            echo "</table>";
        }
        
    } catch (Exception $e) { // Catch generic Exceptions.
        throw new Exception('<strong>No users found.</strong>');
    }
}

?>