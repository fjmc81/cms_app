<?php
// core configuration
include_once "inner/config/core.php";
 
// check if logged in as admin
include_once "inner/controllers/login_checker_user.php";

function html_fns_user()
{ 
    // set page title
    $page_title="User Index";
 
    echo "<div class='col-md-12'>";
 
        // get parameter values, and to prevent undefined index notice
        $action = isset($_GET['action']) ? $_GET['action'] : "";
 
        // tell the user he's already logged in
        if($action=='already_logged_in'){
            echo "<div class='alert alert-info'>";
                echo "<strong>You</strong> are already logged in.";
            echo "</div>";
        }
 
        else if($action=='logged_in_as_admin'){
            echo "<div class='alert alert-info'>";
                echo "<strong>You</strong> are logged in as User.";
            echo "</div>";
        }
 
        echo "<div class='alert alert-info'>";
            echo "Contents of your user section will be here.";
            $manage = isset($_GET['manage']) ? $_GET['manage'] : 'sec-users';
            //here controllers
            switch($manage) {
                default:
                    require_once('html_fns_read_user_entries.php');
                    html_fns_read_user_entries();
                    break;
                case 'user-entries':
                    require_once('html_fns_read_user_entries.php');
                    html_fns_read_user_entries();
                    break;
                case 'sec-users':
                    require_once('html_fns_read_users.php');
                    html_fns_read_users();
                    break;
            }
        echo "</div>";
 
    echo "</div>";
}
?>