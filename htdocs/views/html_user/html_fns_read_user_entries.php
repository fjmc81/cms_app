<?php
 
function html_fns_read_user_entries()
{
    // page given in URL parameter, default page is one
    $pages = isset($_GET['pages']) ? $_GET['pages'] : 1;
    
    // set number of records per page
    $records_per_page = 3;
    
    // calculate for the query LIMIT clause
    $from_record_num = ((int)$records_per_page * $pages) - (int)$records_per_page;

    // include models
    include_once "inner/models/Entry.php";
    include_once "inner/models/User.php";
    
    // instantiate database and objects
    $database = new Database();
    $db = $database->getConnection();
    
    $entry = new Entry($db);  

    try {

        // get database connection
        $database = new Database();
        $db = $database->getConnection();

        // initialize objects
        $entry = new Entry($db);
        
        $q = 'SELECT id, title, excerpt, content 
                FROM entries  
                WHERE entries.creatorId='.$_SESSION["user_id"].'
                ORDER BY id DESC
                '; 

        $stmt = $db->prepare( $q );

        // bind given attribute value
        $stmt->bindParam(1, $entry->title);
        $stmt->bindParam(1, $entry->excerpt);
        $stmt->bindParam(1, $entry->content);
        $stmt->bindParam(1, $entry->creatorId);
    
        // execute the query
        $stmt->execute();

            echo "<table class='table table-hover table-responsive table-bordered'>";
                echo "<tr>";
                    echo "<th>Id</th>";
                    echo "<th>Title</th>";
                    echo "<th>Excerpt</th>";
                    echo "<th>Creator Id</th>";
                    echo "<th></th>";
                    echo "<th></th>";
                    echo "<th></th>";
                echo "</tr>";
        
                while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
        
                    extract($row);
        
                    echo "<tr>";
                        echo "<td>{$id}</td>";
                        echo "<td>{$title}</td>";
                        echo "<td>{$excerpt}</td>";
                        echo "<td>{$_SESSION["user_id"]}</td>";
                        // read, edit and delete buttons
                        echo "<td>";
                            echo "<a href='index.php?page=read&id={$id}' class='btn btn-primary left-margin'>
                            <span class='glyphicon glyphicon-list'></span> Read
                            </a>";
                        echo "</td>";
                        echo "<td>";
                            echo "<a href='index.php?page=update_entry&id={$id}' class='btn btn-info left-margin'>
                            <span class='glyphicon glyphicon-edit'></span> Edit
                            </a>";
                        echo "</td>";    
                        echo "<td>";
                            echo "<a delete-id='{$id}' class='btn btn-danger delete-object'>
                            <span class='glyphicon glyphicon-remove'></span> Delete
                            </a>";
                        echo "</td>";
        
                    echo "</tr>";
        
                }
        
            echo "</table>";    

    } catch (Exception $e) { // Catch generic Exceptions.
        throw new Exception('No content is available to be viewed at this time.');
    }
}

?>