<?php
 
function html_fns_read_all_entries()
{
    // page given in URL parameter, default page is one
    $pages = isset($_GET['pages']) ? $_GET['pages'] : 1;
    
    // set number of records per page
    $records_per_page = 4;
    
    // calculate for the query LIMIT clause
    //$from_record_num = ((int)$records_per_page * (int)$page) - (int)$records_per_page;
    $from_record_num = ((int)$records_per_page * $pages) - (int)$records_per_page;

    // include models
    include_once "inner/config/database.php";
    include_once "inner/models/Entry.php";
    
    // instantiate database and objects
    $database = new Database();
    $db = $database->getConnection();
    
    $entry = new Entry($db);  

    // query products
    $stmt = $entry->readAll($db, $from_record_num, $records_per_page);
    $stmt2 = $entry->readAllNoLimits($db);
    $num = $stmt2->rowCount();
    $total_rows = $num;

    if($num>0){
    

    
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    
                extract($row);
                echo "<div class='d-flex justify-content-around'>";
                echo "<ul class='list-group list-group-flush '>";
                    echo "<li class='list-group-item'><h1>{$title}</h1></li>";
                    echo "<li class='list-group-item'><p>{$excerpt}</p></li>";

                        // read, edit and delete buttons
                        echo "<li class='list-group-item d-flex justify-content-end'>
                        <a href='index.php?page=read&id={$id}' class='btn btn-primary right-margin'>
                            <span class='glyphicon glyphicon-list'></span> Read
                        </a>
                        </li>";
    
                echo "</ul>";
                echo "</div>";
    
            }

    
        // paging buttons will be here
        include_once 'html_fns_paging.php';
    }
    
    // tell the user there are no entries
    else{
        echo "<div class='alert alert-info'>No entries found.</div>";
    }
}

?>