<?php

function html_fns_body($page)
{
    //here controllers
    switch($page) {
        default: //Default
            require_once('html_fns_index.php');
            html_fns_index();
        case '': //When it is null
            require_once('html_fns_index.php');
            html_fns_index();
        case 'home':
            require_once('html_fns_read_all_entries.php');
            html_fns_read_all_entries();
            break;
        case 'login':
            require_once('html_fns_login.php');
            html_fns_login();
            break;
        case 'login_check':
            require_once('inner/controllers/login.php');
            //html_fns_login();
            break;
        case 'logout':
            require_once('html_fns_logout.php');
            html_fns_logout();
        case 'create_entry':
            require_once('html_fns_create_entry.php');
            html_fns_create_entry();
            break;
        case 'create_entry_check':
            require_once('inner/controllers/create_entry.php');
            break;
        case 'delete_entry':
            require_once('html_fns_delete_entry.php');
            html_fns_delete_entry();
            break;
        case 'update_entry':
            require_once('html_fns_update.php');
            html_fns_update();
            break;
        case 'update_entry_check':
            require_once('inner/controllers/update_entry.php');
            break;
        case 'read':
            require_once('html_fns_oneEntry.php');
            html_fns_oneEntry();
            break;
        case 'admin':
            require_once('html_admin/html_fns_admin.php');
            html_fns_admin();
            break;
        case 'user':
            require_once('html_user/html_fns_user.php');
            html_fns_user();
            break;
    }
    
}
?>