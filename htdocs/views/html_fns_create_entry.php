<?php
require_once('inner/controllers/create_entry.php');

function html_fns_create_entry()
{
 
?>

<!-- HTML form for creating a entry -->
<form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"].'?page=create_entry_check');?>" method="post">
  
    <table class='table'>
  
        <tr>
            <td>Title</td>
            <td><input type='text' name='title' class='form-control' required /></td>
        </tr>

        <tr>
            <td>Excerpt</td>
            <td><textarea name='excerpt' class='form-control' required></textarea></td>
        </tr>
  
        <tr>
            <td>Content</td>
            <td><textarea name='content' class='form-control' required></textarea></td>
        </tr>
  
        <tr>
            <td></td>
            <td>
                <input class='idCreatorID' type='text' name='creatorId' class='form-control' value='<?php echo $_SESSION["user_id"]; ?>'/>
            </td>
        </tr>
  
        <tr>
            <td></td>
            <td>
                <button type="submit" class="btn btn-primary">Create</button>
            </td>
        </tr>
  
    </table>
</form>

<?php

}

?>