<?php

require_once('inner/controllers/create_user.php');

function html_fns_registration_user()
{

    ?>

    <form action='<?php echo htmlspecialchars($_SERVER["PHP_SELF"]. "?page=admin&manage=register");?>' method='post' id='register'>    
        <table class='table'>
    
            <tr>
                <td>Username</td>
                <td><input type='text' name='username' value='<?php echo isset($_POST['username']); ?>' class='form-control' required /></td>
            </tr>

            <tr>
                <td>Email</td>
                <td><input name='email' value='<?php echo isset($_POST['email']); ?>' class='form-control' required /></td>
            </tr>
    
            <tr>
                <td>Password</td>
                <td><input name='password' value='<?php echo isset($_POST['password']); ?>' class='form-control' required /></td>
            </tr>

            <tr>
                <td>Access Level</td>
                <td>
                <select name="access_level" class='form-control'>
                    <option value="user">User</option>
                    <option value="admin">Admin</option>
                </select>
                </td>
            </tr>
    
            <tr>
                <td></td>
                <td>
                    <button type="submit" class="btn btn-primary">Update</button>
                </td>
            </tr>
    
        </table>
    </form>

<?php
}
?>