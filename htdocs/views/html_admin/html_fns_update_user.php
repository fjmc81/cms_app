<?php

function html_fns_update_user()
{

    include_once __DIR__."\..\..\inner\models\User.php";
    
    // set page title
    $page_title="Update User";
    
    // include login checker
    $require_login=false;
    include_once "inner/controllers/login_checker.php";
    
    // get ID of the entry to be edited
    $id = isset($_GET['id']) ? $_GET['id'] : die('ERROR: missing ID.');
    
    var_dump($id);

    // get database connection
    $database = new Database();
    $db = $database->getConnection();
    
    // prepare objects
    $user = new User($db);
    
    // set ID property of user to be edited
    $user->id = $id;
    
    // read the details of user to be edited
    $user->update($db);


    // if the form was submitted
    if($_POST){
    
        // set user property values
        $user->username = $_POST['username'];
        $user->email = $_POST['email'];
        $user->pass = $_POST['password'];
        $user->access_level = $_POST['access_level'];

        var_dump($_POST['username']);
        
        // update the user
        if($user->update($db)){
            echo "<div class='alert alert-success alert-dismissable'>";
                echo "User was updated.";
            echo "</div>";
        }
    
        // if unable to update the user, tell the user
        else{
            echo "<div class='alert alert-danger alert-dismissable'>";
                echo "Unable to update user.";
            echo "</div>";
        }
    }
    var_dump($user->username);

    ?>
    
    <form action="<?php echo htmlspecialchars($_SERVER["PHP_SELF"]. "?page=admin&manage=update_user&id={$id}");?>" method="post">
        <table class='table'>
    
            <tr>
                <td>Username</td>
                <td><input type='text' name='username' value='<?php echo $user->username; ?>' class='form-control' required/></td>
            </tr>

            <tr>
                <td>Email</td>
                <td><input type='email' name='email' class='form-control' <?php echo $user->email; ?> required /></td>
            </tr>
    
            <tr>
                <td>Password</td>
                <td><input type='password' name='password' class='form-control' <?php echo $user->pass; ?>  required/></td>
            </tr>

            <tr>
                <td>Access Level</td>
                <td><input type='text' name='access_level' class='form-control' <?php  ?>  required/></td>
            </tr>
    
            <tr>
                <td></td>
                <td>
                    <button type="submit" class="btn btn-primary">Update</button>
                </td>
            </tr>
    
        </table>
    </form>

<?php
}
?>