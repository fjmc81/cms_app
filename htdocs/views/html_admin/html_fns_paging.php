<?php
echo "<ul class='nav'>";
 
$page_url="index.php?page=admin&manage=admin-entries&";

// button for first page
if($pages>1){
    echo "<li class='nav-item'><a class='nav-link' href='index.php?page=admin&manage=admin-entries' title='Go to the first page.'>";
        echo "First Page";
    echo "</a></li>";
}
 
// calculate total number of pages
$total_pages = ceil($total_rows / $records_per_page);
 
// range of links to show
$range = 2;
 
// display links to 'range of pages' around 'current page'
$initial_num = $pages - $range;
$condition_limit_num = ($pages + $range)  + 1;
 
for ($x=$initial_num; $x<$condition_limit_num; $x++) {
     
    // be sure '$x is greater than 0' AND 'less than or equal to the $total_pages'
    if (($x > 0) && ($x <= $total_pages)) {
     
        // current page
        if ($x == $pages) {
            echo "<li class='nav-item active'><a class='nav-link' href=\"#\">$x <span class=\"sr-only\">(current)</span></a></li>";
        } 
         
        // not current page
        else {
            echo "<li class='nav-item'><a class='nav-link' href='{$page_url}pages=$x'>$x</a></li>";
        }
    }
}
 
// button for last page
if($pages<$total_pages){
    echo "<li class='nav-item'><a class='nav-link' href='" .$page_url . "pages={$total_pages}' title='Last page is {$total_pages}.'>";
        echo " Last Page";
    echo "</a></li>";
}
 
echo "</ul>";
?>