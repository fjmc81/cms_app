<?php
// core configuration
include_once "inner/config/core.php";
 
// check if logged in as admin
include_once "inner/controllers/login_checker_admin.php";

//include_once('html_fns_read_users.php');
//include_once('html_fns_read_entries.php');

function html_fns_admin()
{ 
    // set page title
    $page_title="Admin Index";
 
    echo "<div class='col-md-12'>";
 
        // get parameter values, and to prevent undefined index notice
        $action = isset($_GET['action']) ? $_GET['action'] : "";
 
        // tell the user he's already logged in
        if($action=='already_logged_in'){
            echo "<div class='alert alert-info'>";
                echo "<strong>You</strong> are already logged in.";
            echo "</div>";
        }
 
        else if($action=='logged_in_as_admin'){
            echo "<div class='alert alert-info'>";
                echo "<strong>You</strong> are logged in as admin.";
            echo "</div>";
        }
 
        echo "<div class='alert alert-info'>";
            echo "Contents of your admin section will be here.";
            $manage = isset($_GET['manage']) ? $_GET['manage'] : 'admin-users';
            //here controllers
            switch($manage) {
                default:
                    require_once('html_fns_read_entries.php');
                    html_fns_read_entries();
                    break;
                case 'admin-entries':
                    require_once('html_fns_read_entries.php');
                    html_fns_read_entries();
                    break;
                case 'admin-users':
                    require_once('html_fns_read_users.php');
                    html_fns_read_users();
                    break;
                case 'register':
                    require_once('html_fns_registration_user.php');
                    html_fns_registration_user();
                    break;
                case 'update_user':
                    require_once('html_fns_update_user.php');
                    html_fns_update_user();
                    break;
            }
        echo "</div>";
 
    echo "</div>";
}
?>