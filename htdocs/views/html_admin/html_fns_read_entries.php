<?php
 
function html_fns_read_entries()
{
    // page given in URL parameter, default page is one
    $pages = isset($_GET['pages']) ? $_GET['pages'] : 1;
    
    // set number of records per page
    $records_per_page = 5;
    
    // calculate for the query LIMIT clause
    //$from_record_num = ((int)$records_per_page * (int)$page) - (int)$records_per_page;
    $from_record_num = ((int)$records_per_page * $pages) - (int)$records_per_page;

    // include models
    include_once "inner/models/Entry.php";
    
    // instantiate database and objects
    $database = new Database();
    $db = $database->getConnection();
    
    $entry = new Entry($db);  

    // query products
    $stmt = $entry->readAll($db, $from_record_num, $records_per_page);
    $stmt2 = $entry->readAllNoLimits($db);
    $num = $stmt2->rowCount();
    $total_rows = $num;

    if($num>0){
    
        echo "<table class='table table-hover table-responsive table-bordered'>";
            echo "<tr>";
                echo "<th>Id</th>";
                echo "<th>Title</th>";
                echo "<th>Excerpt</th>";
                echo "<th>Creator Id</th>";
                echo "<th></th>";
                echo "<th></th>";
                echo "<th></th>";
            echo "</tr>";
    
            while ($row = $stmt->fetch(PDO::FETCH_ASSOC)){
    
                extract($row);
    
                echo "<tr>";
                    echo "<td>{$id}</td>";
                    echo "<td>{$title}</td>";
                    echo "<td>{$excerpt}</td>";
                    echo "<td>{$creatorId}</td>";
                    // read, edit and delete buttons
                    echo "<td>";
                        echo "<a href='index.php?page=read&id={$id}' class='btn btn-primary left-margin'>
                        <span class='glyphicon glyphicon-list'></span> Read
                        </a>";
                    echo "</td>";
                    echo "<td>";
                        echo "<a href='index.php?page=update_entry&id={$id}' class='btn btn-info left-margin'>
                        <span class='glyphicon glyphicon-edit'></span> Edit
                        </a>";
                    echo "</td>";    
                    echo "<td>";
                        echo "<a delete-id='{$id}' class='btn btn-danger delete-object'>
                        <span class='glyphicon glyphicon-remove'></span> Delete
                        </a>";
                    echo "</td>";
    
                echo "</tr>";
    
            }
    
        echo "</table>";
    
        // paging buttons will be here
        include_once 'html_fns_paging.php';
    }
    
    // tell the user there are no entries
    else{
        echo "<div class='alert alert-info'>No entries found.</div>";
    }
}

?>