<?php
require_once('inner/config/core.php');

function html_fns_menu()
{
    $page_title = isset($page_title) ? $page_title : "Blog App --test!";
?>
<!-- navbar -->
<div class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container-fluid">
        <a class="navbar-brand" href="/cms_app/htdocs/index.php?page=home">Franz App</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
 
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active" <?php echo $page_title=="Index" ? "class='active'" : ""; ?>>
                    <a class="nav-link" href="/cms_app/htdocs/index.php?page=home">Home</a>
                </li>
                <?php
                if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true && $_SESSION['access_level']=='user'){
                ?>
                    <li class="nav-item" <?php echo $page_title=="Create"; ?>>
                        <a class="nav-link" href="/cms_app/htdocs/index.php?page=create_entry" tabindex="-1">Create</a>
                    </li>
                <?php
                }
                if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true && $_SESSION['access_level']=='user'){
                ?>
                <li class="nav-item dropdown" <?php echo $page_title=="user"; ?>>
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    User
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/cms_app/htdocs/index.php?page=user&manage=sec-users">User</a>
                    <a class="dropdown-item" href="/cms_app/htdocs/index.php?page=user&manage=user-entries">Entries</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/cms_app/htdocs/index.php?page=user">Home User</a>
                    </div>
                </li>
                <?php
                }
                if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true && $_SESSION['access_level']=='admin'){
                ?>
                <li class="nav-item dropdown" <?php echo $page_title=="admin"; ?>>
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Admin
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                    <a class="dropdown-item" href="/cms_app/htdocs/index.php?page=admin&manage=admin-users">Users</a>
                    <a class="dropdown-item" href="/cms_app/htdocs/index.php?page=admin&manage=admin-entries">Entries</a>
                    <a class="dropdown-item" href="/cms_app/htdocs/index.php?page=admin&manage=register">New User</a>
                    <div class="dropdown-divider"></div>
                    <a class="dropdown-item" href="/cms_app/htdocs/index.php?page=admin">Home Admin</a>
                    </div>
                </li>
                <?php
                }
                ?>
            </ul>

            <?php
            // check if users / customer was logged in
            // if user was logged in, show "Edit Profile", "Orders" and "Logout" options
            if(isset($_SESSION['logged_in']) && $_SESSION['logged_in']==true){ // && ($_SESSION['access_level']=='user'){
                ?>
                <ul class="nav navbar-nav navbar-right">
                    <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="glyphicon glyphicon-user" aria-hidden="true"></span>
                            <?php echo $_SESSION['username']; ?>
                            <span class="caret"></span>
                        </a>
                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                            <a class="dropdown-item" href="/cms_app/htdocs/index.php?page=logout"><i class="fa fa-sign-out" aria-hidden="true"></i>Logout</a>
                        </div>
                    </li>
                </ul>
                <?php
                } 
                // if user was not logged in, show the "login" and "register" options
                else{
                ?>
                <ul class="nav navbar-nav navbar-right">
                    <li <?php echo $page_title=="Login" ? "class='active'" : ""; ?>>
                        <a href="/cms_app/htdocs/index.php?page=login">
                        <i class="fa fa-sign-in" aria-hidden="true"></i> Log In
                        </a>
                    </li>
                </ul>
                <?php
                } 
            ?>

        </div>
 
    </div>
</div>
<!-- /navbar -->
<?php
}

?>