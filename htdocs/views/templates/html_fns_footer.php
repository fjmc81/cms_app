<?php

function html_fns_footer()
{
?>
    </div><!-- /container -->
    <!-- end HTML page -->

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="d-flex align-items-center">
                    <p class="copyright text-muted">Copyright &copy; Franz app 2021</p>
                </div>
            </div>
        </div>
    </footer>
    
    <!-- Scripts & links JS -->
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
   
    <!-- bootbox library -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/bootbox.js/5.5.2/bootbox.min.js"></script>


    <script>
    // JavaScript for deleting product
    $(document).on('click', '.delete-object', function(){
    
        var id = $(this).attr('delete-id');
        var userid = $(this).attr('delete-user-id');
        var url = 'index.php';
    
        bootbox.confirm({
            message: "<h4>Are you sure?</h4>",
            buttons: {
                confirm: {
                    label: '<span class="glyphicon glyphicon-ok"></span> Yes',
                    className: 'btn-danger'
                },
                cancel: {
                    label: '<span class="glyphicon glyphicon-remove"></span> No',
                    className: 'btn-primary'
                }
            },
            callback: function (result) {
                if(result==true && id){
                    $.post("inner/controllers/delete_entry.php", {
                        object_id: id,
                    }, function(data){
                        $(location).attr("href", "index.php");
                    }).fail(function() {
                        alert('Unable to delete.');
                    });
                }else if(result==true && userid){
                    $.post("inner/controllers/delete_user.php", {
                        object_id: userid,
                    }, function(data){
                        $(location).attr("href", "index.php?page=admin&manage=admin-users");
                    }).fail(function() {
                        alert('Unable to delete.');
                    });
                }
            }
        });
    
        return false;
    });
    </script>
</body>
</html>
<?php
}

?>