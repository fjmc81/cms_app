<?php

function html_fns_header()
{

echo '
    <!DOCTYPE html>
    <html lang="en">
    <head>
    
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
    
        <!-- set the page title, for seo purposes too -->
        <title>CMS App PHP project</title>
    
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" crossorigin="anonymous">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

        <!-- custom css -->
        <link rel="stylesheet" href="public/css/styles.css" media="screen and (min-width: 900px)" />
    
    </head>
    <body>

        <!-- container -->
        <div class="container">
        ';
            
}

?>