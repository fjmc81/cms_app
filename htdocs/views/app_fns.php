<?php
require_once('inner/config/core.php');
require_once('inner/controllers/login_checker.php');

require_once('views/templates/html_fns_header.php');
require_once('views/templates/html_fns_menu.php');

require_once('views/html_fns_body.php');

require_once('views/templates/html_fns_footer.php');
?>