<?php

//include_once('inner/controllers/index.php');

function html_fns_index($action = '')
{

    // core configuration
    include_once "inner/config/core.php";
 
    // set page title
    $page_title="Index";
    
    // include login checker
    $require_login=true;
    //include_once "login_checker.php";
 
    echo "<div class='d-flex justify-content-end'>"; 
        echo "<div class='col-md-9'>";
        
            // to prevent undefined index notice
            $action = isset($_GET['action']) ? $_GET['action'] : "";
        
            // if login was successful
            if($action=='login_success'){
                echo "<div class='alert alert-info'>";
                    echo "<strong>Hi " . $_SESSION['username'] . ", welcome back!</strong>";
                echo "</div>";
            }
        
            // if user is already logged in, shown when user tries to access the login page
            else if($action=='already_logged_in'){
                echo "<div class='alert alert-info'>";
                    echo "<strong>You are already logged in.</strong>";
                echo "</div>";
            }

            // if login was successful
            if($action=='login_check'){
                echo "<div class='alert alert-info'>";
                    echo "<strong>Hi " . $_SESSION['username'] . ", welcome back!</strong>";
                echo "</div>";
            }
        
            // content once logged in        
            // Fetch the three most recent entries:
            try {

                // include models
                include_once "inner/models/Entry.php";
                include_once "inner/models/User.php";
                
                // get database connection
                $database = new Database();
                $db = $database->getConnection();

                // initialize objects
                $entry = new Entry($db);
                
                $q = 'SELECT id, title, excerpt, content 
                        FROM entries  
                        ORDER BY id ASC
                        '; 

                $stmt = $db->prepare( $q );

                // bind given attribute value
                $stmt->bindParam(1, $entry->title);
                $stmt->bindParam(1, $entry->excerpt);
                $stmt->bindParam(1, $entry->content);
            
                // execute the query
                $stmt->execute();
                
                // Check that rows were returned:
                if ($stmt) {

                    // Set the fetch mode:
                    $stmt->setFetchMode(PDO::FETCH_CLASS, 'Entry');

                    // Records will be fetched in the view:
                    while ($entry = $stmt->fetch()) {
                        echo "<article>
                        <h1>{$entry->getTitle()}</h1>
                        <p>{$entry->getExcerpt()}</p>
                        </article>
                        <div class='d-flex justify-content-around'>
                            <a href='index.php?page=read&id={$entry->getId()}' class='btn btn-primary right-margin'>
                                <span class='glyphicon glyphicon-list'></span> Read entry
                            </a>
                        </div>
                        ";
                    }

                } else { // Problem!
                    throw new Exception('No content is available to be viewed at this time.');
                }
                    
            } catch (Exception $e) { // Catch generic Exceptions.
                throw new Exception('No content is available to be viewed at this time.');
            }

        echo "</div>";
    echo "</div>";
    }
?>