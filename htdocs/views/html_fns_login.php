<?php
include_once('inner/controllers/login.php');

function html_fns_login()
{    
    // default to false
    $access_denied=false;

    echo "<div class='d-flex justify-content-center'>";
        echo "<div class='col-sm-6 col-md-4 col-md-offset-4'>";
        
            // get 'action' value in url parameter to display corresponding prompt messages
            $action=isset($_GET['action']) ? $_GET['action'] : "";
            
            // tell the user he is not yet logged in
            if($action =='not_yet_logged_in'){
                echo "<div class='alert alert-danger margin-top-40' role='alert'>Please login.</div>";
            }
            
            // tell the user to login
            else if($action=='please_login'){
                echo "<div class='alert alert-info'>
                    <strong>Please login to access that page.</strong>
                </div>";
            }
            
            // tell the user email is verified
            else if($action=='email_verified'){
                echo "<div class='alert alert-success'>
                    <strong>Your email address have been validated.</strong>
                </div>";
            }
            
            // tell the user if access denied
            if($access_denied){
                echo "<div class='alert alert-danger margin-top-40' role='alert'>
                    Access Denied.<br /><br />
                    Your username or password maybe incorrect
                </div>";
            }
        
            // actual HTML login form
            echo "<div class='account-wall'>";
                echo "<div id='my-tab-content' class='tab-content'>";
                    echo "<div class='tab-pane active' id='login'>";
                        echo "<form class='form-signin' action='" . htmlspecialchars($_SERVER["PHP_SELF"].'?page=login_check') . "' method='post'>";
                            echo "<div class='form-group'>";
                            echo "<input type='text' name='email' class='form-control' placeholder='Email' required autofocus />";
                            echo "</div>";
                            echo "<div class='form-group'>";
                            echo "<input type='password' name='password' class='form-control' placeholder='Password' required />";
                            echo "</div>";
                            echo "<div class='form-group'>";
                            echo "<input type='submit' class='btn btn-lg btn-primary btn-block' value='Log In' />";
                            echo "</div>";
                            echo "</form>";
                    echo "</div>";
                echo "</div>";
            echo "</div>";
        
        echo "</div>";
    echo "</div>";

}
?>