<?php
include_once __DIR__."\..\..\inner/config/database.php";

class User extends Database
{
    // database connection and table name
    private $conn;
    private $table_name = "users";

    //Atributes
    public $id;
    public $username;
    public $email;
    public $pass;
    public $access_level;

    //Constructor here
    //public function __construct($db){
    //    $this->conn = $db;
    //}

    //Setters here
    public function setId($id){
        $this->id = $id;
    }

    public function setUsername($username){
        $this->username = $username;
    }

    public function setEmail($email){
        $this->email = $email;
    }

    public function setAccess_level($access_level){
        $this->access_level = $access_level;
    }

    public function setPass($pass){
        $this->pass = $pass;
    }


    //Getters here
    public function getId(){
        return $this->id;
    }

    public function getUsername(){
        return $this->username;
    }

    public function getEmail(){
        return $this->email;
    }

    public function getPass(){
        return $this->pass;
    }

    public function getAccess_level(){
        return $this->access_level;
    }

    // check if given email exist in the database
    public function emailExists($db){
    
        // query to check if email exists
        $query = "SELECT id, access_level, username, pass
                FROM " . $this->table_name . "
                WHERE email = ?
                LIMIT 0,1";
        
        $stmt = $this->conn = $db;
        // prepare the query
        $stmt = $this->conn->prepare( $query );
    
        // sanitize
        $this->email=htmlspecialchars(strip_tags($this->email));
    
        // bind given email value
        $stmt->bindParam(1, $this->email);
    
        // execute the query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();
    
        // if email exists, assign values to object properties for easy access and use for php sessions
        if($num>0){
    
            // get record details / values
            $row = $stmt->fetch(PDO::FETCH_ASSOC);
            // assign values to object properties
            $this->id = $row['id'];
            $this->access_level = $row['access_level'];
            $this->username = $row['username'];
            $this->pass = $row['pass'];
    
            // return true because email exists in the database
            return true;
        }
    
        // return false if email does not exist in the database
        return false;
    }

    // read all user records
    function readAll($db, $from_record_num, $records_per_page){
    
        // query to read all user records, with limit clause for pagination
        $query = "SELECT
                    id,
                    username,
                    email,
                    pass,
                    access_level
                FROM " . $this->table_name . "
                ORDER BY id DESC
                LIMIT ?, ?";
        
        $stmt = $this->conn = $db;
        // prepare query statement
        $stmt = $this->conn->prepare( $query );
    
        // bind limit clause variables
        $stmt->bindParam(1, $from_record_num, PDO::PARAM_INT);
        $stmt->bindParam(2, $records_per_page, PDO::PARAM_INT);
        
        // execute query
        $stmt->execute();
    
        // return values
        return $stmt;
    }

    // used for paging users
    public function countAll(){
    
        // query to select all user records
        $query = "SELECT id FROM " . $this->table_name . "";
    
        // prepare query statement
        $stmt = $this->conn->prepare($query);
    
        // execute query
        $stmt->execute();
    
        // get number of rows
        $num = $stmt->rowCount();
    
        // return row count
        return $num;
    }

    // create user
    function create($db){
  
        //write query
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                username=:username, email=:email, pass=:pass, access_level=:access_level
                ";

        $stmt = $this->conn = $db;
        $stmt = $this->conn->prepare($query);
  
        // posted values
        $this->username=htmlspecialchars(strip_tags($this->username));
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->pass=htmlspecialchars(strip_tags($this->pass));
        $this->access_level=htmlspecialchars(strip_tags($this->access_level));
  
        // bind values 
        $stmt->bindParam(":username", $this->username);
        $stmt->bindParam(":email", $this->email);
        $stmt->bindParam(":pass", $this->pass);
        $stmt->bindParam(":access_level", $this->access_level);
  
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
  
    }

    // update user
    function update($db){
  
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    username = :username,
                    email = :email,
                    pass = :pass,
                    access_level = :access_level
                WHERE
                    id = :id";
        
        $stmt = $this->conn = $db;
        $stmt = $this->conn->prepare($query);
      
        // posted values
        $this->username=htmlspecialchars(strip_tags($this->username));
        $this->email=htmlspecialchars(strip_tags($this->email));
        $this->pass=htmlspecialchars(strip_tags($this->pass));
        $this->access_level=htmlspecialchars(strip_tags($this->access_level));
        $this->id=htmlspecialchars(strip_tags($this->id));
      
        // bind parameters
        $stmt->bindParam(':username', $this->username);
        $stmt->bindParam(':email', $this->email);
        $stmt->bindParam(':pass', $this->pass);
        $stmt->bindParam(':access_level', $this->access_level);
        $stmt->bindParam(':id', $this->id);
      
        // execute the query
        if($stmt->execute()){
            return true;
        }
        return false;
          
    }

    // delete user
    function delete($db){
  
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
        
        $stmt = $this->conn = $db;
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
      
        if($result = $stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

}

?>