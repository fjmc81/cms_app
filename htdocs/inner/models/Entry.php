<?php
include_once __DIR__."\..\..\inner\config\database.php";

class Entry extends Database
{
    // database connection and table name
    private $conn;
    private $table_name = "entries";

    //Atributes
    public $id;
    public $creatorId;
    public $title;
    public $excerpt;
    public $content;

    //Setters here
    public function setId($id){
        $this->id = $id;
    }

    public function setCreatorId($creatorId){
        $this->creatorId = $creatorId;
    }

    public function setTitle($title){
        $this->title = $title;
    }

    public function setExcerpt($excerpt){
        $this->excerpt = $excerpt;
    }

    public function setContent($content){
        $this->content = $content;
    }


    //Getters here
    function getId() {
        return $this->id;
    }   
    function getCreatorId() {
        return $this->creatorId;
    }   
    function getTitle() {
        return $this->title;
    }
    function getExcerpt() {
        return $this->excerpt;
    }
    function getContent() {
        return $this->content;
    }

    // create entry
    function create($db){
  
        //write query
        $query = "INSERT INTO
                    " . $this->table_name . "
                SET
                creatorId=:creatorId, title=:title, excerpt=:excerpt ,content=:content
                ";

        $stmt = $this->conn = $db;
        $stmt = $this->conn->prepare($query);
  
        // posted values
        $this->creatorId=htmlspecialchars(strip_tags($this->creatorId));
        $this->title=htmlspecialchars(strip_tags($this->title));
        $this->excerpt=htmlspecialchars(strip_tags($this->excerpt));
        $this->content=htmlspecialchars(strip_tags($this->content));
  
        // bind values 
        $stmt->bindParam(":creatorId", $this->creatorId);
        $stmt->bindParam(":title", $this->title);
        $stmt->bindParam(":excerpt", $this->excerpt);
        $stmt->bindParam(":content", $this->content);
  
        if($stmt->execute()){
            return true;
        }else{
            return false;
        }
  
    }

    // read individual entry
    function readOne($db){
  
        $query = "SELECT
                    creatorId, title, excerpt, content
                FROM
                    " . $this->table_name . "
                WHERE
                    id = ?
                LIMIT
                    0,1";
        
        $stmt = $this->conn = $db;
        $stmt = $this->conn->prepare( $query );
        $stmt->bindParam(1, $this->id);
        $stmt->execute();
      
        $row = $stmt->fetch(PDO::FETCH_ASSOC);
      
        $this->creatorId = $row['creatorId'];
        $this->title = $row['title'];
        $this->excerpt = $row['excerpt'];
        $this->content = $row['content'];
    }

    function readAll($db, $from_record_num, $records_per_page){
  
        $query = "SELECT
                    id, creatorId, title, excerpt, content
                FROM
                    " . $this->table_name . "
                ORDER BY
                    id ASC
                LIMIT
                    {$from_record_num}, {$records_per_page}";
        
        $stmt = $this->conn = $db;
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
      
        return $stmt;
    }

    function readAllNoLimits($db){
  
        $query = "SELECT
                    id, creatorId, title, excerpt, content
                FROM
                    " . $this->table_name . "
                ORDER BY
                    id ASC
                ";
        
        $stmt = $this->conn = $db;
        $stmt = $this->conn->prepare( $query );
        $stmt->execute();
      
        return $stmt;
    }

    // update entry
    function update($db){
  
        $query = "UPDATE
                    " . $this->table_name . "
                SET
                    creatorId = :creatorId,
                    title = :title,
                    excerpt = :excerpt,
                    content = :content
                WHERE
                    id = :id";
        
        $stmt = $this->conn = $db;
        $stmt = $this->conn->prepare($query);
      
        // posted values
        $this->creatorId=htmlspecialchars(strip_tags($this->creatorId));
        $this->title=htmlspecialchars(strip_tags($this->title));
        $this->excerpt=htmlspecialchars(strip_tags($this->excerpt));
        $this->content=htmlspecialchars(strip_tags($this->content));
        $this->id=htmlspecialchars(strip_tags($this->id));
      
        // bind parameters
        $stmt->bindParam(':creatorId', $this->creatorId);
        $stmt->bindParam(':title', $this->title);
        $stmt->bindParam(':excerpt', $this->excerpt);
        $stmt->bindParam(':content', $this->content);
        $stmt->bindParam(':id', $this->id);
      
        // execute the query
        if($stmt->execute()){
            return true;
        }
        return false;
          
    }

    // delete entry
    function delete($db){
  
        $query = "DELETE FROM " . $this->table_name . " WHERE id = ?";
        
        $stmt = $this->conn = $db;
        $stmt = $this->conn->prepare($query);
        $stmt->bindParam(1, $this->id);
      
        if($result = $stmt->execute()){
            return true;
        }else{
            return false;
        }
    }

}



?>