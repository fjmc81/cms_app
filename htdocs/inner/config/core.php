<?php
// show error reporting
error_reporting(E_ALL);
 
// start php session
session_start();
 
// set your default time-zone
date_default_timezone_set('Europe/London');
 
// home page url
$home_url="http://localhost/cms_app/htdocs/";

?>