<?php
// core configuration
include_once "inner/config/core.php";
 
// set page title
$page_title = "Login";
 
// include login checker
$require_login=true;
include_once "login_checker.php";
 
// default to false
$access_denied=false;
 
// if the login form was submitted
if($_POST){
    // include classes
    //include_once "inner/config/database.php";
    include_once "inner/models/User.php";
    
    // get database connection
    $database = new Database();
    $db = $database->getConnection();
    
    // initialize objects
    $user = new User($db);
    
    // check if email and password are in the database
    $user->email=$_REQUEST['email'];
    $user->pass=$_REQUEST['password'];
    
    // check if email exists, also get user details using this emailExists() method
    $email_exists = $user->emailExists($db);
    
        if ($email_exists && $user->pass == $_POST['password']){
    
            // if it is, set the session value to true
            $_SESSION['logged_in'] = true;
            $_SESSION['user_id'] = $user->id;
            $_SESSION['access_level'] = $user->access_level;
            $_SESSION['username'] = htmlspecialchars($user->username, ENT_QUOTES, 'UTF-8') ;
            
            //var_dump($_SESSION['access_level']);
            //die();

            // if access level is 'Admin', redirect to admin section
            if($_SESSION['access_level']=='admin'){
                header("Location: /cms_app/htdocs/index.php?page=admin&action=login_success");
            }
        
            // else, redirect only to 'Customer' section
            else{
                header("Location: /cms_app/htdocs/index.php?page=home&action=login_success");
            }
        }
        
        
        // if username does not exist or password is wrong
        else{
            $access_denied=true;
        }
    }
?>