<?php
// core configuration
include_once "inner/config/core.php";
// include models
include_once "inner/Models/User.php";
 
// set page title
$page_title="Create User";
 
// include login checker
$require_login=false;
include_once "login_checker.php";

// if the form was submitted - PHP OOP CRUD Tutorial
if($_POST){

    $database = new Database();
    $db = $database->getConnection();

    $user = new User($db);
  
    // set user property values
    $user->username = $_POST['username'];
    $user->email = $_POST['email'];
    $user->pass = $_POST['password'];
    $user->access_level = $_POST['access_level'];
  
    // create the user
    if($user->create($db)){
        echo "<div class='alert alert-success'>User was created.</div>";
    }
  
    // if unable to create the user, tell the user
    else{
        echo "<div class='alert alert-danger'>Unable to create user.</div>";
    }
}
?>