<?php
// core configuration
include_once "inner/config/core.php";
// include models
//include_once "inner/config/database.php";
include_once "inner/Models/Entry.php";
 
// set page title
$page_title="Index";
 
// include login checker
$require_login=false;
include_once "login_checker.php";

// if the form was submitted - PHP OOP CRUD Tutorial
if($_POST){

    $database = new Database();
    $db = $database->getConnection();

    $entry = new Entry($db);
  
    // set product property values
    $entry->creatorId = $_POST['creatorId'];
    $entry->title = $_POST['title'];
    $entry->excerpt = $_POST['excerpt'];
    $entry->content = $_POST['content'];
  
    // create the product
    if($entry->create($db)){
        echo "<div class='alert alert-success'>Entry was created.</div>";
    }
  
    // if unable to create the entry, tell the user
    else{
        echo "<div class='alert alert-danger'>Unable to create entry.</div>";
    }
}
?>