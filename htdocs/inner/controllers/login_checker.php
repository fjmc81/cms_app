<?php
// login checker for 'user' access level

if(isset($require_login) && $require_login==true){
    // if user not yet logged in, redirect to login page
    if(!isset($_SESSION['access_level'])){
        header("Location: /cms_app/htdocs/index.php?page=login&action=please_login");
    }
}
 
// if it was the 'login' page but the user was already logged in
else if(isset($page_title) && ($page_title=="Login" || $page_title=="Sign Up")){
    // if user not yet logged in, redirect to login page
    if(isset($_SESSION['access_level']) && $_SESSION['access_level']=="user"){
        header("Location: /cms_app/htdocs/index.php?page=home&action=already_logged_in");
    }
}
 
else{
    // no problem, stay on current page
}
?>