<?php
// login checker for 'admin' access level
 
// if the session value is empty, he is not yet logged in, redirect him to login page
if(empty($_SESSION['logged_in'])){
    header("Location: /cms_app/htdocs/index.php?page=login&action=please_login");
}
 
// if access level is 'Admin', redirect him to login page
//else if($_SESSION['access_level']=="admin"){
//    header("Location: /cms_app/htdocs/index.php?page=admin&action=logged_in_as_admin");
//}
// if access level was not 'Admin' but is 'User", redirect him to home page
else if($_SESSION['access_level']!="user"){
    header("Location: /cms_app/htdocs/index.php?page=home&action=already_logged_in");
}
else{
    //no problem, stay there.
}
?>